# Stats Importer for Wordle

Simple HTML page that you can deploy alongside a Wordle mirror to manually import your stats.

## Short Instructions

1. Upload this page to the server hosting your Wordle mirror.
2. Open the page and walk thru the wizard steps to enter your stats.
3. ????
4. PROFIT!!!

### Example One

You have been playing official Wordle, but now would like to use your mirror hosted at example.com.

1. Open the official Wordle.
2. Open the Stats Importer hosted on example.com on a new tab.
3. Follow the prompts.
4. When done, you can start playing on example.com with your stats restored.

### Example Two

You already have a mirror hosted at example.com, and now you have a new phone/device and would like to transfer over your stats to it.

1. Open your mirror copy of Wordle hosted at example.com on the old device.
2. Open the Stats Importer hosted on example.com on the new device.
3. Follow the prompts.
4. When done, your stats will be imported into your new device, and you can continue playing on example.com

## Screnshots

![Welcome Message](https://gitlab.com/jqr404/stats-importer-for-wordle/-/wikis/uploads/cd3c4dfebaf25521a29300295161ba85/image.png)

![Stats Review](https://gitlab.com/jqr404/stats-importer-for-wordle/-/wikis/uploads/25889a35cbfc80c1050bf4e7c7fb5f31/image.png)

![Import Complete](https://gitlab.com/jqr404/stats-importer-for-wordle/-/wikis/uploads/58cee7671b781a88c707e402f1c2c345/image.png)


## Caveats

- The Stats Importer overwrites your stats on the device for the site where the game is hosted. In other words, if you open the stats screen on your game, and you don't want to lose the information there, *do not* run this tool. On the other hand, if you have no stats yet, then it's safe to run the tool. Just to be safe, screenshot your data before running this tool.
- The Stats Importer has been tested, but not extensively so. Besides the caveat above, there is little risk in testing it, but still, do so at your own risk.
- I am just a hobbyist, as will be obvious to any experienced programmers looking at my code. But hey, it works!

## More Information

I will be adding more detailed instructions to [the Wiki](https://gitlab.com/jqr404/stats-importer-for-wordle/-/wikis/home) as question arise.

If you run into any trouble, please [open an issue](https://gitlab.com/jqr404/stats-importer-for-wordle/-/issues/new) to let me know.

Have any improvements? [Merge/Pull Requests](https://gitlab.com/jqr404/stats-importer-for-wordle/-/merge_requests/new) are welcome and very much appreciated.

### Alternatives

- [Bookmarklet](https://medium.com/@archy_bold/transfer-your-wordle-save-state-stats-between-different-browsers-and-devices-38afcc4f8a8c) by Simon Archer
- [Wordle Stats Generation Tool](https://aneejian.com/get-wordle-streak-stats-back/) by Aneejian
- *Your solution here.*

---

*Wordle was created by Josh Wardle and is the property of the New York Times. This project does not distribute any copyrighted material, nor does it contain instructions on how to obtain copyrighted material.*
